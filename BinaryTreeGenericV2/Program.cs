﻿using System;

namespace BinaryTreeGenericPhoom
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BinaryTree<int> tree = new BinaryTree<int>();
                bool IsShowMenu = true;
                while (IsShowMenu)
                {
                    Console.Clear();
                    Console.WriteLine("======BinarySearchTree=====");
                    Console.WriteLine("Press 1 Insert Value in tree");
                    Console.WriteLine("Press 2 List Value in tree inOrder");
                    Console.WriteLine("Press 3 Search Value in tree");
                    Console.WriteLine("Press 0 Exit");
                    Console.Write("\r\nSelect an option: ");

                    string key = Console.ReadLine();
                    switch (key)
                    {
                        case "1":
                            Console.Clear();
                            Console.WriteLine("You select to insert value in tree!");
                            Console.WriteLine("Please give integer value that you wanna insert to the tree");
                            var value = tree.GetInput<int>(Console.ReadLine());
                            tree.Insert(value);
                            Console.WriteLine($"Value: {value} added in the tree");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.Write("List of Tree : ");
                            tree.DisplayTree();
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.WriteLine("Please give integer value that you wanna search in the tree");
                            var findvalue = tree.GetInput<int>(Console.ReadLine());
                            var node = tree.FindNode(findvalue, tree.Root);
                            if (node != null)
                            {
                                Console.WriteLine($"Found Node:{node.Key}");
                                Console.ReadLine();
                            }
                            else
                            {
                                Console.WriteLine($"Not found Node:{findvalue}");
                                Console.ReadLine();
                            }

                            break;
                        case "0":
                            Console.Clear();
                            Console.WriteLine("Exit Program");
                            IsShowMenu = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }

    public class BinaryTree<T> where T : IComparable<T>
    {
        public class Node
        {
            public T Key;
            public Node Left, Right;

            public Node(T item)
            {
                Key = item;
                Left = null;
                Right = null;
            }
        }

        public Node Root;

        public BinaryTree()
        {
            Root = null;
        }

        public void Insert(T key)
        {
            Root = InsertValue(Root, key);
        }

        Node InsertValue(Node root, T key)
        {
            if (root == null)
            {
                root = new Node(key);
                return root;
            }

            if (Root.Key.CompareTo(key) > 0)
            {
                root.Left = InsertValue(root.Left, key);
            }
            else
            {
                root.Right = InsertValue(root.Right, key);
            }

            return root;
        }

        public void Sort()
        {
            SortTree(Root);
        }

        void SortTree(Node root)
        {
            if (root != null)
            {
                SortTree(root.Left);
                Console.Write(root.Key + " ");
                SortTree(root.Right);
            }
        }

        public void DisplayTree()
        {
            Sort();
        }

        public Node FindNode(T value, Node parent)
        {
            if (parent != null)
            {
                if (parent.Key.CompareTo(value) == 0) return parent;
                if (parent.Key.CompareTo(value) > 0)
                {
                    return FindNode(value, parent.Left);
                }
                else
                {
                    return FindNode(value, parent.Right);
                }
            }

            return null;
        }

        public T GetInput<T>(string input)
        {
            return (T) Convert.ChangeType(input, typeof(T));
        }
    }
}