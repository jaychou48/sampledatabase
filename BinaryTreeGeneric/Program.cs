﻿using System;

namespace BinaryTreeGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BinaryTree<int> tree = new BinaryTree<int>();
                bool showMenu = true;
                while (showMenu)
                {
                    showMenu = tree.TreeMenu();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            
        }
    }

    public class BinaryTree<T> where T : IComparable<T>
    {
        public bool TreeMenu()
        {
            Console.Clear();
            Console.WriteLine("======BinarySearchTree=====");
            Console.WriteLine("Press 1 Insert Value in tree");
            Console.WriteLine("Press 2 List Value in tree inOrder");
            Console.WriteLine("Press 3 Search Value in tree");
            Console.WriteLine("Press 0 Exit");
            Console.Write("\r\nSelect an option: ");

            string key = Console.ReadLine();
            switch (key)
            {
                case "1":
                    InsertOpt();
                    return true;
                case "2":
                    Console.Write("List of Tree : ");
                    DisplayTree();
                    return true;
                case "3":
                    Search();
                    return true;
                case "0":
                    Exit();
                    return false;
                default:
                    return true;
            }
        }

        class Node
        {
            public T Key;
            public Node Left, Right;

            public Node(T item)
            {
                Key = item;
                Left = null;
                Right = null;
            }
        }

        private Node _root;

        public BinaryTree()
        {
            _root = null;
        }

        public void Insert(T key)
        {
            _root = InsertValue(_root, key);
        }

        Node InsertValue(Node root, T key)
        {
            if (root == null)
            {
                root = new Node(key);
                return root;
            }

            if (this._root.Key.CompareTo(key) > 0)
            {
                root.Left = InsertValue(root.Left, key);
            }
            else
            {
                root.Right = InsertValue(root.Right, key);
            }

            return root;
        }

        public void Sort()
        {
            SortTree(_root);
        }

        void SortTree(Node root)
        {
            if (root != null)
            {
                SortTree(root.Left);
                Console.Write(root.Key + " ");
                SortTree(root.Right);
            }
        }

        void DisplayTree()
        {
            Sort();
            Console.ReadLine();
        }

        Node findNode(T value, Node parent)
        {
            if (parent != null)
            {
                if (parent.Key.CompareTo(value) == 0) return parent;
                if (parent.Key.CompareTo(value) > 0)
                {
                    return findNode(value, parent.Left);
                }
                else
                {
                    return findNode(value, parent.Right);
                }
            }

            return null;
        }

        void Search()
        {
            Console.Clear();
            Console.WriteLine("Please give integer value that you wanna search in the tree");
            var key = GetInput<T>();
            Node node = findNode(key, _root);
            if (node != null)
            {
                Console.WriteLine($"Found Node:{node.Key}");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine($"Not found Node:{key}");
                Console.ReadLine();
            }
        }

        void InsertOpt()
        {
            Console.Clear();
            Console.WriteLine("You select to insert value in tree!");
            Console.WriteLine("Please give integer value that you wanna insert to the tree");
            var key = GetInput<T>();
            Insert(key);
            Console.WriteLine($"Value: {key} added in the tree");
            Console.ReadLine();
        }

        private static T GetInput<T>()
        {
            string input = Console.ReadLine();
            return (T) Convert.ChangeType(input, typeof(T));
        }

        void Exit()
        {
            Console.Clear();
            Console.WriteLine("Exit Program");
        }
    }
}