﻿using System;
using System.Collections.Generic;

namespace TreePersonGeneric
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Console.WriteLine("-----Menu-----");
            // Console.WriteLine("1. Add Person");
            // Console.WriteLine("2. List Person");
            // Console.WriteLine("3. Get Person by ID");
            // Console.WriteLine("4. Search Person by name");
            // Console.WriteLine("5. Search Person by age");
            // Console.WriteLine("Q. Quit");
            
            //Find By Id
            Person person1 = new Person(1,"Jack",35);
            Person person2 = new Person(2,"David",18);
            Person person3 = new Person(3,"Morgan",75);
            BinaryTree<int, Person> PersonIdIndexRecursive = new BinaryTree<int, Person>();
            BinaryTree<int, Person> PersonIdIndexIteration = new BinaryTree<int, Person>();
            PersonIdIndexRecursive.InsertValueByRecursive(1, person1);
            PersonIdIndexRecursive.InsertValueByRecursive(3, person2);
            PersonIdIndexRecursive.InsertValueByRecursive(2, person3);
            PersonIdIndexIteration.InsertValueByIteration(1, person1);
            PersonIdIndexIteration.InsertValueByIteration(3, person2);
            PersonIdIndexIteration.InsertValueByIteration(2, person3);
            var PersonNode1 = PersonIdIndexRecursive.FindNodeRecursive(PersonIdIndexRecursive.Root,1);
            var PersonNode2 = PersonIdIndexRecursive.FindNodeRecursive(PersonIdIndexRecursive.Root,5);
            var PersonNode3 = PersonIdIndexRecursive.FindNodeRecursive(PersonIdIndexRecursive.Root,3);
            var PersonNode4 = PersonIdIndexIteration.FindNodeIteration(1);
            var PersonNode5 = PersonIdIndexIteration.FindNodeIteration(5);
            var PersonNode6 = PersonIdIndexIteration.FindNodeIteration(3);
            if ((PersonNode1.Key == PersonNode4.Key) && (PersonNode2 == PersonNode5) && (PersonNode3.Key == PersonNode6.Key))
            {
                Console.WriteLine("Iteration and Recursive is equality");
            }
        }
    }
    
    public class Person
    {
        public int Id, Age;
        public string Name;

        public Person(int id,string name,int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }
    }

    public class BinaryTree<TKey, TValue> where TKey : IComparable<TKey>
    {
        // Node<int, Person> PersonIdIndex;
        // Node<string, int> PersonNameIndex;
        // Node<int, int> PersonAgeIndex;
        public class Node<TKey, TValue>
        {
            public TKey Key;
            public TValue Value;
            public Node<TKey, TValue> Parent, Left, Right;

            public Node(TKey key, TValue value)
            {
                Value = value;
                Key = key;
            }
        }
        
        public Node<TKey, TValue> Root;
        public BinaryTree()
        {
            Root = null;
        }

        public void InsertValueByRecursive(TKey key, TValue value)
        {
            Root = InsertRecursive(Root,key, value);
        }
        
        public void InsertValueByIteration(TKey key, TValue value)
        {
            Root = InsertIteration(key, value);
        }

        Node<TKey, TValue> InsertRecursive(Node<TKey, TValue> root, TKey key, TValue value)
        {
            if (root == null)
            {
                root = new Node<TKey, TValue>(key, value);
                return root;
            }

            if (root.Key.CompareTo(key) > 0)
            {
                root.Left = InsertRecursive(root.Left, key, value);
            }
            else
            {
                root.Right = InsertRecursive(root.Right, key, value);
            }

            return root;
        }

        Node<TKey, TValue> InsertIteration(TKey key, TValue value)
        {
            Node<TKey, TValue> parent = null;
            Node<TKey, TValue> current = Root;
            int compare = 0;
            while (current != null)
            {
                parent = current;
                compare = current.Key.CompareTo(key);
                current = compare < 0 ? current.Right : current.Left;
            }

            Node<TKey, TValue> newNode = new Node<TKey, TValue>(key, value);
            if (parent != null)
                if (compare < 0)
                    parent.Right = newNode;
                else
                    parent.Left = newNode;
            else
                Root = newNode;
            newNode.Parent = parent;

            return Root;
        }

        public Node<TKey, TValue> FindNodeIteration(TKey key)
        {
            Node<TKey, TValue> current = Root;
            while (current != null)
            {
                int compare = current.Key.CompareTo(key);
                if (compare == 0)
                    return current;
                if (compare < 0)
                    current = current.Right;
                else
                    current = current.Left;
            }

            return null;
        }
        
        public Node<TKey, TValue> FindNodeRecursive(Node<TKey, TValue> root,TKey key)
        {
            if (root != null)
            {
                if (root.Key.CompareTo(key) == 0) return root;
                if (root.Key.CompareTo(key) > 0)
                {
                    return FindNodeRecursive(root.Left,key);
                }
                else
                {
                    return FindNodeRecursive(root.Right,key);
                }
            }

            return null;
        }
    }
}