﻿using System;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class BinarySearchTree
    {
        public static void Main(string[] args)
        {
            BinarySearchTree tree = new BinarySearchTree();
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = TreeMenu(tree);
            }
        }

        private static bool TreeMenu(BinarySearchTree tree)
        {
            Console.Clear();
            Console.WriteLine("======BinarySearchTree=====");
            Console.WriteLine("Press 1 Insert Value in tree");
            Console.WriteLine("Press 2 List Value in tree inOrder");
            Console.WriteLine("Press 3 Search Value in tree");
            Console.WriteLine("Press 0 Exit");
            Console.Write("\r\nSelect an option: ");

            string key = Console.ReadLine();
            switch (key)
            {
                case "1":
                    insertOpt(tree);
                    return true;
                case "2":
                    Console.Write("List of Tree : ");
                    displayTree(tree);
                    return true;
                case "3":
                    search(tree);
                    return true;
                case "0":
                    exit();
                    return false;
                default:
                    return true;
            }
        }

        static void insertOpt(BinarySearchTree tree)
        {
            Console.Clear();
            Console.WriteLine("You select to insert value in tree!");
            Console.WriteLine("Please give integer value that you wanna insert to the tree");
            string key = Console.ReadLine();
            if (checkIsInteger(key))
            {
                int number = Int32.Parse(key);
                tree.insert(number);
                Console.WriteLine($"Value: {number} already add in the tree");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Please insert only integer value");
                Console.ReadLine();
            }
        }

        static void displayTree(BinarySearchTree tree)
        {
            tree.sort();
            Console.ReadLine();
        }

        static void exit()
        {
            Console.Clear();
            Console.WriteLine("Exit Program");
        }

        static bool checkIsInteger(string value)
        {
            int number;
            return Int32.TryParse(value, out number);
        }

        class Node
        {
            public int key;
            public Node left, right;
            public Node(int item)
            {
                key = item;
                left = null;
                right = null;
            }
        }

        Node root;

        public BinarySearchTree()
        {
            root = null;
        }

        void insert(int key)
        {
            root = insertValue(root, key);
        }

        Node insertValue(Node root, int key)
        {
            if (root == null)
            {
                root = new Node(key);
                return root;
            }

            if (key < root.key)
            {
                root.left = insertValue(root.left, key);
            }
            else if (key > root.key)
            {
                root.right = insertValue(root.right, key);
            }

            return root;
        }

        static void search(BinarySearchTree tree)
        {
            Console.Clear();
            Console.WriteLine("Please give integer value that you wanna search in the tree");
            string key = Console.ReadLine();
            if (checkIsInteger(key))
            {
                int number = Int32.Parse(key);
                Node node = findNode(number, tree.root);
                if (node != null)
                {
                    Console.WriteLine($"Found Node:{node.key}");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine($"Not found Node:{key}");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Please insert only integer value");
                Console.ReadLine();
            }
        }

        static Node findNode(int value, Node parent)
        {
            if (parent != null)
            {
                if (value == parent.key) return parent;
                if (value < parent.key)
                {
                    return findNode(value, parent.left);
                }
                else
                {
                    return findNode(value, parent.right);
                }
            }
        
            return null;
        }

        void sort()
        {
            sortTree(root);
        }

        void sortTree(Node root)
        {
            if (root != null)
            {
                sortTree(root.left);
                Console.Write(root.key + " ");
                sortTree(root.right);
            }
        }
    }
}